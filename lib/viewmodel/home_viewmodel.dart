import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech_1/main.dart';
import 'package:oech_1/model/message.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../model/rider.dart';

/*
Описание: Логика главных экранов
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class HomeViewModel extends ChangeNotifier {
  List<Rider> riderList = [];

  List<Message> messageList = [
    Message("Hello, Please check your phone, I just booked you to deliver my stuff", true),
    Message("Thank you for contacting me.", false),
    Message("I am already on my way to the pick up venue.", false),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
    Message("Alright, I wll be waiting", true),
  ];

  /*
  Описание: Метод отправки транзакции на сервер (тест)
  Дата создания: 29.07.2023
  Автор: Хасанов Георгий
  */
  Future<Map<String, dynamic>> addTransaction() async {
    try {
      int id = 0;

      final d = await supabase.from("transactions").insert(
        {
          "title": "Transaction",
          "price": 10000.00,
        },
      );

      final data = await getLastTransaction();

      return data;
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );
      return {};
    }
  }

  /*
  Описание: Метод получения транзакции с сервера (тест)
  Дата создания: 29.07.2023
  Автор: Хасанов Георгий
  */
  Future<Map<String, dynamic>> getLastTransaction() async {
    try {
      int id = 0;

      final data = await supabase.from("transactions").select<List<dynamic>>();

      return data.last;
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR 2"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );
      return {};
    }
  }

  /*
  Описание: Метод обновления транзакции на сервере (тест)
  Дата создания: 29.07.2023
  Автор: Хасанов Георгий
  */
  Future<Map<String, dynamic>> updateTransaction(int id) async {
    try {
      final data = await supabase
          .from("transactions")
          .upsert(
            {
              "id": id,
              "title": "UPDATED",
              "price": 10000.00,
            },
          )
          .eq("id", id)
          .single();

      return {
        "id": id,
        "title": "UPDATED",
        "price": 10001.00,
      };
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR 3"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );
      return {};
    }
  }

  /*
  Описание: Метод загрузки доставщиков с сервера
  Дата создания: 30.07.2023
  Автор: Хасанов Георгий
  */
  Future<List<Rider>> loadRiders() async {
    try {
      // final data = await supabase.from("riders").select<List<dynamic>>();

      // for (var rider in data) {
      //   riderList.add(Rider(rider["firstName"], rider["lastName"], rider["carModel"], rider["regNumber"], rider["gender"], rider["rating"]));
      // }

      riderList.add(Rider("firstName", "lastName", "carModel", "R-regNumber", "M", 4, "+123 123 123", "Online"));
      riderList.add(Rider("firstName2", "lastName2", "carModel2", "R-regNumber2", "M", 5, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 3, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 0, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 3, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 2, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 3, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 4, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 3, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 0, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 2, "+123 123 123", "Online"));
      riderList.add(Rider("firstName3", "lastName3", "carModel3", "R-regNumber3", "F", 1, "+123 123 123", "Online"));

      return riderList;
    } catch (e) {
      return List<Rider>.empty();
    }
  }
}
