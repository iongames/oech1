import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech_1/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class LoginViewModel extends ChangeNotifier {
  /*
  Описание: Функция повторного входа в аккаунт
  Дата создания: 28.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> checkUser() async {
    return supabase.auth.currentSession != null;
  }

  /*
  Описание: Функция входа в аккаунт
  Дата создания: 28.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> logIn(String email, String password) async {
    try {
      AuthResponse response = await supabase.auth.signInWithPassword(email: email, password: password);

      Get.back();
      return response.session != null;
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );

      return false;
    }
  }

/*
  Описание: Функция входа в аккаунт через Google
  Дата создания: 28.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> logInWithGoogle() async {
    try {
      bool response = await supabase.auth.signInWithOAuth(Provider.google);

      Get.back();
      return response;
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );

      return false;
    }
  }

  /*
  Описание: Функция создания аккаунта
  Дата создания: 28.07.2023
  Автор: Хасанов Георгий
  */
  Future<bool> signUp(String email, String password, String phone, String name) async {
    try {
      AuthResponse response = await supabase.auth.signUp(email: email, password: password, data: {"Full Name": name, "Phone": phone});

      Get.back();
      return true;
    } catch (e) {
      Get.back();
      Get.dialog(
        AlertDialog(
          title: Text("ERROR"),
          content: Text(e.toString()),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("OK"))
          ],
        ),
      );

      return false;
    }
  }
}
