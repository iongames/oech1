import 'package:flutter/material.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'home_screen.dart';

/*
Дата создания: 27.06.2023
Описание: Класс создания нового пароля
Автор: Георгий Хасанов
*/
class NewPasswordScreen extends StatefulWidget {
  const NewPasswordScreen({super.key});

  @override
  State<NewPasswordScreen> createState() => _NewPasswordScreenState();
}

/*
Дата создания: 27.06.2023
Описание: State New Password Screen
Автор: Георгий Хасанов
*/
class _NewPasswordScreenState extends State<NewPasswordScreen> {
  final TextEditingController password1Controller = TextEditingController();
  final TextEditingController password2Controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 110),
                  Text(
                    "New Password",
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter new password",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "New Password",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: password1Controller,
                    placeholder: "***********",
                    inputType: TextInputType.emailAddress,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "New Password",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: password2Controller,
                    placeholder: "***********",
                    inputType: TextInputType.emailAddress,
                  ),
                ],
              ),
              SizedBox(height: 93),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () async {
                              if (password1Controller.text == password2Controller.text) {
                                showDialog(
                                  context: context,
                                  builder: (builder) {
                                    return Center(child: CircularProgressIndicator());
                                  },
                                );

                                await Future.delayed(Duration(seconds: 1));

                                Navigator.pop(context);

                                if (true) {
                                  Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                                }
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (builder) {
                                      return AlertDialog(
                                        title: Text("ERROR"),
                                        content: Text("Пароли не совпадают!"),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text("OK"),
                                          ),
                                        ],
                                      );
                                    });
                              }
                            },
                            text: "Log in"),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
