import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/login_screen.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'otp_screen.dart';

/*
Дата создания: 27.06.2023
Описание: Класс экрана восстановления пароля
Автор: Георгий Хасанов
*/
class ForgotScreen extends StatefulWidget {
  const ForgotScreen({super.key});

  @override
  State<ForgotScreen> createState() => _ForgotScreenState();
}

/*
Дата создания: 27.06.2023
Описание: State Forgot Password Screen
Автор: Георгий Хасанов
*/
class _ForgotScreenState extends State<ForgotScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 110),
                  Text(
                    "Forgot Password",
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter your email address",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: emailController,
                    placeholder: "***********@mail.com",
                    inputType: TextInputType.emailAddress,
                  ),
                ],
              ),
              SizedBox(height: 64),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () async {
                              if (RegExp(r"^[a-z0-9]*@[a-z0-9]*\.[a-z0-9]{2,}$").hasMatch(emailController.text)) {
                                showDialog(
                                  context: context,
                                  builder: (builder) {
                                    return Center(child: CircularProgressIndicator());
                                  },
                                );

                                await Future.delayed(Duration(seconds: 1));

                                Navigator.pop(context);

                                if (true) {
                                  Navigator.push(context, MaterialPageRoute(builder: (_) => OTPScreen()));
                                }
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (builder) {
                                      return AlertDialog(
                                        title: Text("ERROR"),
                                        content: Text("Неправильный формат E-Mail!"),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text("OK"),
                                          ),
                                        ],
                                      );
                                    });
                              }
                            },
                            text: "Send OTP"),
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  InkWell(
                    onTap: () async {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(text: "Remember password? Back to", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.grayColor)),
                        TextSpan(text: "Sign in", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.primaryColor)),
                      ]),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
