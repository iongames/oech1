import 'package:flutter/material.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'new_password_screen.dart';

/*
Дата создания: 27.06.2023
Описание: Класс подтверждения кода с почты
Автор: Георгий Хасанов
*/
class OTPScreen extends StatefulWidget {
  const OTPScreen({super.key});

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

/*
Дата создания: 27.06.2023
Описание: State OTP Screen
Автор: Георгий Хасанов
*/
class _OTPScreenState extends State<OTPScreen> {
  final TextEditingController code1controller = TextEditingController();
  final TextEditingController code2controller = TextEditingController();
  final TextEditingController code3controller = TextEditingController();
  final TextEditingController code4controller = TextEditingController();
  final TextEditingController code5controller = TextEditingController();
  final TextEditingController code6controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(height: 76),
                  Text(
                    "OTP Verification",
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Enter the 6 digit numbers sent to your email",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 70),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CodeTextField(
                    controller: code1controller,
                    placeholder: "",
                  ),
                  CodeTextField(
                    controller: code2controller,
                    placeholder: "",
                  ),
                  CodeTextField(
                    controller: code3controller,
                    placeholder: "",
                  ),
                  CodeTextField(
                    controller: code4controller,
                    placeholder: "",
                  ),
                  CodeTextField(
                    controller: code5controller,
                    placeholder: "",
                  ),
                  CodeTextField(
                    controller: code6controller,
                    placeholder: "",
                  ),
                ],
              ),
              SizedBox(height: 12),
              InkWell(
                onTap: () async {
                  showDialog(
                    context: context,
                    builder: (builder) {
                      return Center(child: CircularProgressIndicator());
                    },
                  );

                  await Future.delayed(Duration(seconds: 1));

                  Navigator.pop(context);

                  if (true) {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => OTPScreen()));
                  }
                },
                child: RichText(
                    text: TextSpan(children: [
                  TextSpan(
                      text: "If you didn’t receive code, ",
                      style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w400, color: AppColors.grayColor)),
                  TextSpan(text: "Resend", style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w400, color: AppColors.primaryColor)),
                ])),
              ),
              SizedBox(height: 84),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () async {
                              showDialog(
                                context: context,
                                builder: (builder) {
                                  return Center(child: CircularProgressIndicator());
                                },
                              );

                              await Future.delayed(Duration(seconds: 1));

                              Navigator.pop(context);

                              if (true) {
                                Navigator.push(context, MaterialPageRoute(builder: (_) => NewPasswordScreen()));
                              }
                            },
                            text: "Set New Password"),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
