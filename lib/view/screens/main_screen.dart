import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/main.dart';
import 'package:oech_1/view/screens/book_rider_screen.dart';
import 'package:oech_1/view/screens/send_package_screen.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';
import 'package:oech_1/view/widgets/user_card.dart';
import 'package:oech_1/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../../model/service_item.dart';
import '../colors.dart';
import '../widgets/service_card.dart';
import 'home_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late HomeViewModel viewModel;

  final TextEditingController _searchController = TextEditingController();

  final List<Widget> imageList = [
    Image.asset("assets/images/fr_50.png"),
    Image.asset("assets/images/fr_51.png"),
    Image.asset("assets/images/fr_52.png"),
    Image.asset("assets/images/fr_53.png"),
    Image.asset("assets/images/fr_54.png"),
  ];

  final List<ServiceItem> serviceList = [
    ServiceItem("Customer Care", "Our customer care service line is available from 8 -9pm week days and 9 - 5 weekends - tap to call us today",
        "assets/images/service_1.svg"),
    ServiceItem("Send a package", "Request for a driver to pick up or deliver your package for you", "assets/images/service_2.svg"),
    ServiceItem("Fund your wallet", "To fund your wallet is as easy as ABC, make use of our fast technology and top-up your wallet today",
        "assets/images/service_3.svg"),
    ServiceItem("Book a rider", "Search for available rider within your area", "assets/images/service_4.svg"),
    ServiceItem(
        "Enroll as a rider",
        "A chance for you to earn as you become one of our delivery agents, enroll and get the necessary trainings from our crew to get started.",
        "assets/images/service_5.svg"),
    ServiceItem(
        "Refer and earn", "Refer a friend to our platform and stand the chance of winning lots of goodies plus free delivery", "assets/images/service_6.svg"),
  ];

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 24),
                  SearchTextField(controller: _searchController, placeholder: "Search services"),
                  SizedBox(height: 24),
                  UserCard(name: supabase.auth.currentUser != null ? supabase.auth.currentUser!.userMetadata!["Full Name"] : ""),
                  SizedBox(height: 39),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Special for you",
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: AppColors.secondaryColor),
                      ),
                      SvgPicture.asset(
                        "assets/icons/ic_next.svg",
                        color: AppColors.secondaryColor,
                      )
                    ],
                  ),
                  SizedBox(height: 7),
                  Container(
                    height: 64,
                    child: ListView.builder(
                      itemCount: 5,
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (itemBuilder, index) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 12),
                          child: imageList[index],
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 29),
                  Text(
                    "What would you like to do",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).primaryColor),
                  ),
                ],
              ),
            ),
            SizedBox(height: 12),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: GridView.builder(
                  clipBehavior: Clip.hardEdge,
                  itemCount: 6,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                  itemBuilder: (itemBuilder, index) {
                    return ServiceCard(
                      serviceItem: serviceList[index],
                      onTap: () async {
                        switch (index) {
                          case 1:
                            Navigator.push(context, MaterialPageRoute(builder: (_) => SendPackageScreen()));
                            break;
                          case 2:
                            final data = await viewModel.addTransaction();

                            print(await viewModel.updateTransaction(data["id"]));
                            break;
                          case 3:
                            Navigator.push(context, MaterialPageRoute(builder: (_) => BookRiderScreen()));
                            break;
                          default:
                        }
                      },
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
