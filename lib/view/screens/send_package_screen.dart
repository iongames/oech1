import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/package_info.dart';
import 'package:oech_1/view/screens/success_screen.dart';
import 'package:oech_1/view/shapes.dart';
import 'package:oech_1/view/widgets/app_button.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';

import '../colors.dart';

/*
Описание: Класс экрана отправки товаров
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class SendPackageScreen extends StatefulWidget {
  const SendPackageScreen({super.key});

  @override
  State<SendPackageScreen> createState() => _SendPackageScreenState();
}

/*
Описание: State класса экрана отправки товаров
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _SendPackageScreenState extends State<SendPackageScreen> {
  TextEditingController addressStartController = TextEditingController();
  TextEditingController stateStartController = TextEditingController();
  TextEditingController phoneStartController = TextEditingController();
  TextEditingController othersStartController = TextEditingController();

  TextEditingController addressEndController = TextEditingController();
  TextEditingController stateEndController = TextEditingController();
  TextEditingController phoneEndController = TextEditingController();
  TextEditingController othersEndController = TextEditingController();

  TextEditingController itemsController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController worthController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text("Send a package", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 43),
                Row(
                  children: [
                    SvgPicture.asset("assets/icons/ic_loc.svg", color: AppColors.primaryColor),
                    SizedBox(width: 8),
                    Text("Origin Details"),
                  ],
                ),
                SizedBox(height: 5),
                PackageTextField(controller: addressStartController, placeholder: "Address"),
                SizedBox(height: 5),
                PackageTextField(controller: stateStartController, placeholder: "State,Country"),
                SizedBox(height: 5),
                PackageTextField(controller: phoneStartController, placeholder: "Phone number"),
                SizedBox(height: 5),
                PackageTextField(controller: othersStartController, placeholder: "Others"),
                SizedBox(height: 39),
                Row(
                  children: [
                    SvgPicture.asset("assets/icons/ic_dest.svg", color: AppColors.primaryColor),
                    SizedBox(width: 8),
                    Text("Destination Details"),
                  ],
                ),
                SizedBox(height: 5),
                PackageTextField(controller: addressEndController, placeholder: "Address"),
                SizedBox(height: 5),
                PackageTextField(controller: stateEndController, placeholder: "State,Country"),
                SizedBox(height: 5),
                PackageTextField(controller: phoneEndController, placeholder: "Phone number"),
                SizedBox(height: 5),
                PackageTextField(controller: othersEndController, placeholder: "Others"),
                SizedBox(height: 17),
                InkWell(
                  onTap: () {},
                  child: Row(
                    children: [
                      SvgPicture.asset("assets/icons/ic_add.svg", color: AppColors.primaryColor),
                      SizedBox(width: 8),
                      Text(
                        "Add destination",
                        style: TextStyle(fontSize: 12, color: AppColors.grayColor),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 13),
                Text(
                  "Package Details",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                ),
                SizedBox(height: 8),
                PackageTextField(controller: itemsController, placeholder: "package items"),
                SizedBox(height: 5),
                PackageTextField(controller: weightController, placeholder: "Weight of item(kg)"),
                SizedBox(height: 5),
                PackageTextField(controller: worthController, placeholder: "Worth of Items"),
                SizedBox(height: 39),
                Text(
                  "Select delivery type",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                ),
                SizedBox(height: 16),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        width: 159,
                        height: 75,
                        child: Card(
                          margin: EdgeInsets.zero,
                          elevation: 5,
                          shadowColor: Colors.black.withOpacity(0.5),
                          color: AppColors.primaryColor,
                          child: InkWell(
                            onTap: () {
                              PackageInfo pInfo = PackageInfo(
                                  addressStartController.text,
                                  stateStartController.text,
                                  phoneStartController.text,
                                  othersStartController.text,
                                  addressEndController.text,
                                  stateEndController.text,
                                  phoneEndController.text,
                                  othersEndController.text,
                                  itemsController.text,
                                  weightController.text,
                                  "123-123-123");

                              Navigator.push(context, MaterialPageRoute(builder: (_) => ConfirmSendAPackageScreen(packageInfo: pInfo)));
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                              child: Column(
                                children: [
                                  SvgPicture.asset("assets/icons/ic_time.svg", color: Colors.white),
                                  SizedBox(height: 10),
                                  Text(
                                    "Instant delivery",
                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: Container(
                        width: 159,
                        height: 75,
                        child: Card(
                          margin: EdgeInsets.zero,
                          elevation: 5,
                          shadowColor: Colors.black.withOpacity(0.5),
                          color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                          child: InkWell(
                            onTap: () {},
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                              child: Column(
                                children: [
                                  SvgPicture.asset("assets/icons/ic_calendar.svg", color: AppColors.grayColor),
                                  SizedBox(height: 10),
                                  Text(
                                    "Scheduled delivery",
                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 85),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ConfirmSendAPackageScreen extends StatefulWidget {
  final PackageInfo packageInfo;

  const ConfirmSendAPackageScreen({super.key, required this.packageInfo});

  @override
  State<ConfirmSendAPackageScreen> createState() => _ConfirmSendAPackageScreenState();
}

class _ConfirmSendAPackageScreenState extends State<ConfirmSendAPackageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text("Send a package", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                Text(
                  "Package Information",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 8),
                Text(
                  "Origin details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.packageInfo.startAddress} ${widget.packageInfo.startCountry}",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.packageInfo.startPhone,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Destination details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.packageInfo.endAddress} ${widget.packageInfo.endCountry}",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.packageInfo.endPhone,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Other details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package Items",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.items,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Weight of items",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.weight,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tracking Number",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.tracking_number,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 37),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 8),
                Text(
                  "Charges",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Delivery Charges",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N2,500.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Instant delivery",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N300.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tax and Service Charges",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N200.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 9),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package total",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N3000.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 46),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        onTap: () {},
                        text: "Edit package",
                        background: Theme.of(context).scaffoldBackgroundColor,
                        borderShape: AppShapes.outlinedButtonShape,
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: AppColors.primaryColor),
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: AppButton(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => SuccessScreen(packageInfo: widget.packageInfo)));
                        },
                        text: "Make payment",
                        background: AppColors.primaryColor,
                        borderShape: AppShapes.buttonShape,
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
