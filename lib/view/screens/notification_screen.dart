import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../colors.dart';

/*
Описание: Класс добавления метода оплаты
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

/*
Описание: State Класса добавления метода оплаты
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text("Notification", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  SizedBox(height: 120),
                  SvgPicture.asset(
                    "assets/icons/ic_notification.svg",
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                    width: 83,
                    height: 83,
                  ),
                  SizedBox(height: 18),
                  Text(
                    "You have no notifications",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
