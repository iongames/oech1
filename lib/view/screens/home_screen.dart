import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/profile_screen.dart';
import 'package:oech_1/view/screens/track_screen.dart';
import 'package:oech_1/view/screens/wallet_screen.dart';

import '../colors.dart';
import 'main_screen.dart';

/*
Описание: Главный экран с навигацией
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class HomeScreen extends StatefulWidget {
  final int? startIndex;

  const HomeScreen({super.key, this.startIndex});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

/*
Описание: State Главного экрана с навигацией
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _HomeScreenState extends State<HomeScreen> {
  List<Widget> _screens = [
    MainScreen(),
    WalletScreen(),
    TrackScreen(),
    ProfileScreen(),
  ];

  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();

    if (widget.startIndex != null) {
      _selectedIndex = widget.startIndex!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Theme.of(context).primaryColor,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        currentIndex: _selectedIndex,
        showUnselectedLabels: true,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/home.svg",
              color: _selectedIndex == 0
                  ? AppColors.primaryColor
                  : Theme.of(context).brightness == Brightness.dark
                      ? Colors.white
                      : AppColors.grayColor,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/wallet.svg",
              color: _selectedIndex == 1
                  ? AppColors.primaryColor
                  : Theme.of(context).brightness == Brightness.dark
                      ? Colors.white
                      : AppColors.grayColor,
            ),
            label: "Wallet",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/track.svg",
              color: _selectedIndex == 2
                  ? AppColors.primaryColor
                  : Theme.of(context).brightness == Brightness.dark
                      ? Colors.white
                      : AppColors.grayColor,
            ),
            label: "Track",
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/icons/profile.svg",
              color: _selectedIndex == 3
                  ? AppColors.primaryColor
                  : Theme.of(context).brightness == Brightness.dark
                      ? Colors.white
                      : AppColors.grayColor,
            ),
            label: "Profile",
          ),
        ],
      ),
      body: _screens[_selectedIndex],
    ));
  }
}
