import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/home_screen.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';

import '../../model/package_info.dart';
import '../colors.dart';
import '../shapes.dart';
import '../widgets/app_button.dart';

class SuccessScreen extends StatefulWidget {
  final PackageInfo packageInfo;

  const SuccessScreen({super.key, required this.packageInfo});

  @override
  State<SuccessScreen> createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen> with TickerProviderStateMixin {
  bool loaded = false;
  String asset = "assets/images/loading.png";

  late AnimationController _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500))..repeat();

  @override
  void initState() {
    super.initState();

    Timer timer = Timer.periodic(Duration(seconds: 2), (timer) {
      timer.cancel();
      _controller.stop();

      setState(() {
        loaded = true;
        asset = "assets/images/success.png";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            SizedBox(height: 99),
            AnimatedBuilder(
              animation: _controller,
              builder: (builder, _child) {
                return Transform.rotate(
                  angle: _controller.value * -6,
                  child: _child,
                );
              },
              child: Image.asset(asset),
            ),
            SizedBox(height: 100),
            loaded
                ? Text(
                    "Transaction Successful",
                    style: TextStyle(fontSize: 24, color: Theme.of(context).hintColor),
                    textAlign: TextAlign.center,
                  )
                : SizedBox(),
            Text(
              "Your rider is on the way to your destination",
              style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(text: "Tracking Number ", style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor)),
                  TextSpan(text: widget.packageInfo.tracking_number, style: TextStyle(fontSize: 14, color: AppColors.primaryColor)),
                ],
              ),
            ),
            SizedBox(height: 120),
            Row(
              children: [
                Expanded(
                  child: AppButton(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen(startIndex: 2)));
                    },
                    text: "Track my item",
                    background: AppColors.primaryColor,
                    borderShape: AppShapes.buttonShape,
                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                  ),
                )
              ],
            ),
            SizedBox(height: 8),
            Row(
              children: [
                Expanded(
                  child: AppButton(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                    },
                    text: "Go back to homepage",
                    background: Theme.of(context).scaffoldBackgroundColor,
                    borderShape: AppShapes.outlinedButtonShape,
                    textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: AppColors.primaryColor),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    ));
  }
}

/*
Описание: Экран успешной доставки заказа
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class DeliverySuccessScreen extends StatefulWidget {
  final PackageInfo packageInfo;

  const DeliverySuccessScreen({super.key, required this.packageInfo});

  @override
  State<DeliverySuccessScreen> createState() => _DeliverySuccessScreenState();
}

/*
Описание: State Экрана успешной доставки заказа
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class _DeliverySuccessScreenState extends State<DeliverySuccessScreen> with TickerProviderStateMixin {
  bool loaded = false;
  String asset = "assets/images/loading.png";

  late AnimationController _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 1000))..repeat();

  final TextEditingController _feedbackController = TextEditingController();

  @override
  void initState() {
    super.initState();

    Timer timer = Timer.periodic(Duration(seconds: 2), (timer) {
      timer.cancel();
      _controller.stop();

      setState(() {
        loaded = true;
        asset = "assets/images/success.png";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 99),
              AnimatedBuilder(
                animation: _controller,
                builder: (builder, _child) {
                  return Transform.rotate(
                    angle: _controller.value * -6,
                    child: _child,
                  );
                },
                child: Image.asset(asset),
              ),
              SizedBox(height: 100),
              loaded
                  ? Text(
                      "Delivery Successful",
                      style: TextStyle(fontSize: 24, color: Theme.of(context).hintColor),
                      textAlign: TextAlign.center,
                    )
                  : SizedBox(),
              SizedBox(height: 8),
              loaded
                  ? Text(
                      "Your Item has been delivered successfully",
                      style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                    )
                  : SizedBox(),
              SizedBox(height: 65),
              Text(
                "Rate Rider",
                style: TextStyle(fontSize: 14, color: AppColors.primaryColor),
              ),
              SizedBox(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(5, (index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: SvgPicture.asset(
                      "assets/icons/ic_star.svg",
                      color: AppColors.grayColor,
                    ),
                  );
                }),
              ),
              SizedBox(height: 36),
              FeedbackTextField(controller: _feedbackController, placeholder: "Add feedback"),
              SizedBox(height: 76),
              Row(
                children: [
                  Expanded(
                    child: AppButton(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                      },
                      text: "Done",
                      background: AppColors.primaryColor,
                      borderShape: AppShapes.buttonShape,
                      textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
