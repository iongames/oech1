import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/rider.dart';

import '../colors.dart';

/*
Описание: Экран звонка доставщику
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class CallRiderScreen extends StatefulWidget {
  final Rider rider;

  const CallRiderScreen({super.key, required this.rider});

  @override
  State<CallRiderScreen> createState() => _CallRiderScreenState();
}

/*
Описание: State Экрана звонка доставщику
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class _CallRiderScreenState extends State<CallRiderScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                SizedBox(height: 85),
                Card(
                  elevation: 0,
                  margin: EdgeInsets.zero,
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                  child: Image.asset(
                    "assets/images/profile_default.png",
                    fit: BoxFit.cover,
                    width: 84,
                    height: 84,
                  ),
                ),
                SizedBox(height: 9),
                Text(
                  "${widget.rider.lastName} ${widget.rider.firstName}",
                  style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.w700,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.primaryColor,
                  ),
                ),
                SizedBox(height: 9),
                Text(
                  widget.rider.riderNumber,
                  style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.w700,
                    color: AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 9),
                Text(
                  "calling...",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.primaryColor,
                  ),
                ),
                SizedBox(height: 80),
                Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                  margin: EdgeInsets.zero,
                  clipBehavior: Clip.antiAlias,
                  color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : AppColors.grayColor,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 55, vertical: 50),
                    child: Column(
                      children: [
                        SvgPicture.asset(
                          "assets/icons/call_buttons.svg",
                          color: Theme.of(context).hintColor,
                        ),
                        SizedBox(height: 50),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: SvgPicture.asset("assets/icons/end_call.svg"),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
