import 'package:flutter/material.dart';
import 'package:oech_1/model/onboard_item.dart';
import 'package:oech_1/view/widgets/onboard_component.dart';

class OnboardScreen extends StatefulWidget {
  const OnboardScreen({super.key});

  @override
  State<OnboardScreen> createState() => _OnboardScreenState();
}

class _OnboardScreenState extends State<OnboardScreen> {
  final List<OnboardItem> onboardItems = [
    OnboardItem("Quick Delivery At Your Doorstep", "Enjoy quick pick-up and delivery to your destination", "assets/images/onboard_1.png"),
    OnboardItem("Flexible Payment", "Different modes of payment either before and after delivery without stress", "assets/images/onboard_2.png"),
    OnboardItem("Real-time Tracking", "Track your packages/items from the comfort of your home till final destination", "assets/images/onboard_3.png"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: PageView.builder(
          itemCount: 3,
          itemBuilder: (itemBuilder, index) {
            return OnboardComponent(onboardItem: onboardItems[index], itemIndex: index);
          },
        ),
      ),
    );
  }
}
