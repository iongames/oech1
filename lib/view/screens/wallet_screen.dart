import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/widgets/profile_card.dart';

import '../colors.dart';

/*
Описание: Класс экрана кошелька
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class WalletScreen extends StatefulWidget {
  const WalletScreen({super.key});

  @override
  State<WalletScreen> createState() => _WalletScreenState();
}

/*
Описание: State Класса экрана кошелька
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _WalletScreenState extends State<WalletScreen> {
  bool _hide = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Wallet", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
        backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
        elevation: 5,
        shadowColor: Colors.black.withOpacity(0.5),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 36),
            ProfileCard(
              balance: "N10,712:00",
              hide: _hide,
              onIconTap: () {
                setState(() {
                  _hide = !_hide;
                });
              },
            ),
            SizedBox(height: 45),
            Card(
              elevation: 0,
              color: Theme.of(context).cardColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48),
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Text(
                      "Top Up",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightTextColor,
                      ),
                    ),
                    SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                              width: 48,
                              height: 48,
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1000),
                                ),
                                color: AppColors.primaryColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(11),
                                  child: SvgPicture.asset(
                                    "assets/icons/ic_bank.svg",
                                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            Text(
                              "Bank",
                              style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightTextColor),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              width: 48,
                              height: 48,
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1000),
                                ),
                                color: AppColors.primaryColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(11),
                                  child: SvgPicture.asset(
                                    "assets/icons/ic_transfer.svg",
                                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            Text(
                              "Transfer",
                              style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightTextColor),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              width: 48,
                              height: 48,
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1000),
                                ),
                                color: AppColors.primaryColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(11),
                                  child: SvgPicture.asset(
                                    "assets/icons/ic_card.svg",
                                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            Text(
                              "Card",
                              style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightTextColor),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
            SizedBox(height: 45),
            Text(
              "Transaction History",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
            ),
            SizedBox(height: 24),
            Expanded(
                child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List.generate(10, (index) {
                  return TransactionCard(title: "Delivery fee", date: "July 7, 2022", sum: "-N2,000.00", type: false);
                }),
              ),
            ))
          ],
        ),
      ),
    ));
  }
}
