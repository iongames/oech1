import 'package:flutter/material.dart';
import 'package:flutter_osm_plugin/flutter_osm_plugin.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/package_info.dart';
import 'package:oech_1/view/screens/package_info.dart';
import 'package:oech_1/view/widgets/app_button.dart';
import 'package:oech_1/view/widgets/status_item.dart';

import '../colors.dart';

/*
Описание: Класс экрана отслеживания заказа
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class TrackScreen extends StatefulWidget {
  const TrackScreen({super.key});

  @override
  State<TrackScreen> createState() => _TrackScreenState();
}

/*
Описание: State Класс экрана отслеживания заказа
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _TrackScreenState extends State<TrackScreen> {
  final MapController _mapController = MapController(initPosition: GeoPoint(latitude: 0, longitude: 0));

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(height: 320, child: OSMFlutter(controller: _mapController)),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 42),
                  Text(
                    "Tracking Number",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 24),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset("assets/icons/ic_sun.svg"),
                      SizedBox(width: 8),
                      Text(
                        "R-7458-4567-4434-5854",
                        style: TextStyle(fontSize: 16, color: AppColors.primaryColor),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  Text(
                    "Package Status",
                    style: TextStyle(fontSize: 16, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 24),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(4, (index) {
                      return StatusItem(
                        value: false,
                        title: "Courier requested",
                        date: "July 7 2022 08:00am",
                        hasLine: index != 3,
                      );
                    }),
                  ),
                  SizedBox(height: 40),
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => PackageInfoScreen(
                                          packageInfo: PackageInfo("1", "1", "1", "1", "1", "1", "1", "endOthers", "items", "weight", "tracking_number"))));
                            },
                            text: "View Package Info"),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
