import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:oech_1/view/screens/login_screen.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';
import 'package:oech_1/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import 'home_screen.dart';

/*
Дата создания: 27.06.2023
Описание: Класс экрана регистрации
Автор: Георгий Хасанов
*/
class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

/*
Дата создания: 27.06.2023
Описание: State Register Screen
Автор: Георгий Хасанов
*/
class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController nameContoroller = TextEditingController();
  final TextEditingController phoneContoroller = TextEditingController();
  final TextEditingController emailContoroller = TextEditingController();
  final TextEditingController passwordContoroller = TextEditingController();

  bool _passwordVisible = false;
  bool _termsAgree = false;

  late LoginViewModel viewModel;

  /*
  Описание: Инициализация класса RegisterScreen
  Дата создания: 28.07.2023
  Автор: Хасанов Георгий
  */
  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 36),
                  Text(
                    "Create an account",
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Complete the sign up process to get started",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 30),
                  Text(
                    "Full Name",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: nameContoroller,
                    placeholder: "Abecd   fsgh",
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Phone Number",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: phoneContoroller,
                    placeholder: "000000000000",
                    inputType: TextInputType.number,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: emailContoroller,
                    placeholder: "***********@mail.com",
                    inputType: TextInputType.emailAddress,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Password",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: passwordContoroller,
                    placeholder: "**********",
                    obscureText: _passwordVisible,
                    suffixIcon: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              _passwordVisible = !_passwordVisible;
                            });
                          },
                          child: SvgPicture.asset(
                            "assets/icons/ic_hide.svg",
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: [
                      Checkbox(
                        value: _termsAgree,
                        onChanged: (value) {
                          setState(() {
                            _termsAgree = !_termsAgree;
                          });
                        },
                        activeColor: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
                      ),
                      InkWell(
                        onTap: () {},
                        child: Container(
                          width: MediaQuery.of(context).size.width - 116,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "By ticking this box, you agree to our ",
                                  style: Theme.of(context).textTheme.headlineSmall!.copyWith(fontWeight: FontWeight.w400),
                                ),
                                TextSpan(
                                  text: "Terms and conditions and private policy",
                                  style: Theme.of(context).textTheme.headlineSmall!.copyWith(color: AppColors.warningColor, fontWeight: FontWeight.w400),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 64),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () async {
                              if (RegExp(r"^[a-z0-9]*@[a-z0-9]*\.[a-z0-9]{2,}$").hasMatch(emailContoroller.text)) {
                                if (_termsAgree) {
                                  showDialog(
                                    context: context,
                                    builder: (builder) {
                                      return Center(child: CircularProgressIndicator());
                                    },
                                  );

                                  bool result =
                                      await viewModel.signUp(emailContoroller.text, passwordContoroller.text, phoneContoroller.text, nameContoroller.text);

                                  if (result) {
                                    Navigator.push(context, MaterialPageRoute(builder: (_) => LoginScreen()));
                                  }
                                } else {
                                  showDialog(
                                      context: context,
                                      builder: (builder) {
                                        return AlertDialog(
                                          title: Text("ERROR"),
                                          content: Text("Необходимо принять соглашение!"),
                                          actions: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text("OK"),
                                            ),
                                          ],
                                        );
                                      });
                                }
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (builder) {
                                      return AlertDialog(
                                        title: Text("ERROR"),
                                        content: Text("Неправильный формат E-Mail!"),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text("OK"),
                                          ),
                                        ],
                                      );
                                    });
                              }
                            },
                            text: "Sign Up"),
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  InkWell(
                    onTap: () async {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(text: "Already have an account? ", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.grayColor)),
                        TextSpan(text: "Sign in", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.primaryColor)),
                      ]),
                    ),
                  )
                ],
              ),
              SizedBox(height: 30),
              Column(
                children: [
                  Text(
                    "or sign in using",
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(fontWeight: FontWeight.w400),
                  ),
                  SizedBox(height: 8),
                  InkWell(
                    onTap: () async {
                      showDialog(
                        context: context,
                        builder: (builder) {
                          return Center(child: CircularProgressIndicator());
                        },
                      );

                      bool result = await viewModel.logInWithGoogle();

                      if (result) {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                      }
                    },
                    child: SvgPicture.asset("assets/icons/ic_google.svg"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    ));
  }
}
