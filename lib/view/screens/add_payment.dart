import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/widgets/payment_card.dart';

import '../colors.dart';

/*
Описание: Класс добавления метода оплаты
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class AddPaymentMethodScreen extends StatefulWidget {
  const AddPaymentMethodScreen({super.key});

  @override
  State<AddPaymentMethodScreen> createState() => _AddPaymentMethodScreenState();
}

/*
Описание: State Класса добавления метода оплаты
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _AddPaymentMethodScreenState extends State<AddPaymentMethodScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text("Add payment method", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            children: [
              SizedBox(height: 67),
              PaymentCard(
                title: "Pay with wallet",
                desc: "complete the payment using your e wallet",
              ),
              SizedBox(height: 12),
              PaymentCard(
                title: "Credit / debit card",
                desc: "add new card",
              ),
              SizedBox(height: 12),
              Expanded(child: ListView.builder(itemBuilder: (itemBuilder, index) {}))
            ],
          ),
        ),
      ),
    );
  }
}
