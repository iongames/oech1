import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/rider.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';
import 'package:oech_1/view/widgets/chat_item.dart';
import 'package:oech_1/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../colors.dart';

class ChatScreen extends StatefulWidget {
  final Rider rider;

  const ChatScreen({super.key, required this.rider});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  late HomeViewModel viewModel;

  final TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 70,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(),
              Row(
                children: [
                  Card(
                    elevation: 0,
                    margin: EdgeInsets.zero,
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                    child: Image.asset(
                      "assets/images/profile_default.png",
                      width: 43,
                      height: 43,
                    ),
                  ),
                  SizedBox(width: 8),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${widget.rider.lastName} ${widget.rider.firstName}",
                        style: TextStyle(fontSize: 14, color: Theme.of(context).hintColor),
                      ),
                      SizedBox(height: 3),
                      Text(
                        widget.rider.online,
                        style: TextStyle(fontSize: 12, color: AppColors.primaryColor),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(),
              InkWell(
                onTap: () {},
                child: SvgPicture.asset("assets/icons/ic_phone.svg"),
              )
            ],
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: Stack(children: [
          SingleChildScrollView(
            child: Column(
              children: List.generate(viewModel.messageList.length, (index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 6),
                  child: Row(
                    mainAxisAlignment: viewModel.messageList[index].type ? MainAxisAlignment.end : MainAxisAlignment.start,
                    children: [
                      ChatItem(text: viewModel.messageList[index].message, type: viewModel.messageList[index].type),
                    ],
                  ),
                );
              }),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 24,
            right: 24,
            child: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 13),
                child: Row(
                  children: [
                    SvgPicture.asset("assets/icons/ic_emoji.svg"),
                    SizedBox(width: 7),
                    Expanded(
                      child: S5TextField(
                        controller: _textController,
                        onChanged: (value) {
                          setState(() {});
                        },
                        placeholder: "Enter message",
                        icon: "assets/icons/ic_micro.svg",
                      ),
                    ),
                    SvgPicture.asset("assets/icons/ic_triangle.svg"),
                  ],
                ),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
