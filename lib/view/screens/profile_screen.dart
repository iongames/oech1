import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:oech_1/main.dart';
import 'package:oech_1/view/screens/notification_screen.dart';

import '../colors.dart';
import '../widgets/profile_card.dart';
import 'add_payment.dart';

/*
Описание: Класс экрана профиля аккаунта
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

/*
Описание: State Класса экрана профиля аккаунта
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class _ProfileScreenState extends State<ProfileScreen> {
  bool hide = false;
  bool darkThemeEnabled = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text("Profile", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
        elevation: 5,
        shadowColor: Colors.black.withOpacity(0.5),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              SizedBox(height: 36),
              ProfileCard(
                balance: "N10,712:00",
                hide: hide,
                onIconTap: () {
                  setState(() {
                    hide = !hide;
                  });
                },
              ),
              SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Enable dark Mode",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                  CupertinoSwitch(
                    value: darkThemeEnabled,
                    activeColor: AppColors.primaryColor,
                    onChanged: (value) {
                      setState(() {
                        darkThemeEnabled = !darkThemeEnabled;

                        if (darkThemeEnabled) {
                          Get.changeThemeMode(ThemeMode.dark);
                        } else {
                          Get.changeThemeMode(ThemeMode.light);
                        }
                      });
                    },
                  )
                ],
              ),
              SizedBox(height: 19),
              ItemCard(
                asset: "assets/icons/profile.svg",
                title: "Edit Profile",
                desc: "Name, phone no, address, email ...",
                onTap: () {},
              ),
              SizedBox(height: 12),
              ItemCard(
                asset: "assets/icons/ic_cert.svg",
                title: "Statements & Reports",
                desc: "Download transaction details, orders, deliveries",
                onTap: () {},
              ),
              SizedBox(height: 12),
              ItemCard(
                asset: "assets/icons/ic_notification.svg",
                title: "Notification Settings",
                desc: "mute, unmute, set location & tracking setting",
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) => NotificationScreen()));
                },
              ),
              SizedBox(height: 12),
              ItemCard(
                asset: "assets/icons/wallet.svg",
                title: "Card & Bank account settings",
                desc: "change cards, delete card details",
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) => AddPaymentMethodScreen()));
                },
              ),
              SizedBox(height: 12),
              ItemCard(
                asset: "assets/icons/ic_partner.svg",
                title: "Referrals",
                desc: "check no of friends and earn",
                onTap: () {},
              ),
              SizedBox(height: 12),
              ItemCard(
                asset: "assets/icons/ic_image.svg",
                title: "About Us",
                desc: "know more about us, terms and conditions",
                onTap: () {},
              ),
              SizedBox(height: 12),
              LogOutCard(onTap: () {})
            ],
          ),
        ),
      ),
    ));
  }
}
