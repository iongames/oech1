import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/rider_profile_screen.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';
import 'package:oech_1/viewmodel/home_viewmodel.dart';
import 'package:provider/provider.dart';

import '../../model/rider.dart';
import '../colors.dart';
import '../widgets/rider_item.dart';

/*
Описание: Экран списка доставщиков
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class BookRiderScreen extends StatefulWidget {
  const BookRiderScreen({super.key});

  @override
  State<BookRiderScreen> createState() => _BookRiderScreenState();
}

/*
Описание: State Экрана списка доставщиков
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class _BookRiderScreenState extends State<BookRiderScreen> {
  late HomeViewModel viewModel;

  final TextEditingController _searchController = TextEditingController();

  late Future<List<Rider>> _loadRiders;

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);

    _loadRiders = viewModel.loadRiders();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text(
            "Book a Rider",
            style: TextStyle(
              fontSize: 16,
              color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
            ),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 24),
              S5TextField(
                controller: _searchController,
                placeholder: "Search for a driver",
                onChanged: (value) {
                  setState(() {});
                },
              ),
              SizedBox(height: 16),
              Expanded(
                child: FutureBuilder(
                    future: _loadRiders,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: viewModel.riderList.length,
                          shrinkWrap: true,
                          itemBuilder: (itemBuilder, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: RiderItem(
                                rider: viewModel.riderList[index],
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (_) => RiderScreen(rider: viewModel.riderList[index])));
                                },
                              ),
                            );
                          },
                        );
                      } else {
                        return const Center(child: CircularProgressIndicator());
                      }
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
