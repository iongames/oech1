import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/success_screen.dart';

import '../../model/package_info.dart';
import '../colors.dart';
import '../shapes.dart';
import '../widgets/app_button.dart';

/*
Описание: Экран с информацией о доставке
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class PackageInfoScreen extends StatefulWidget {
  final PackageInfo packageInfo;

  const PackageInfoScreen({super.key, required this.packageInfo});

  @override
  State<PackageInfoScreen> createState() => _PackageInfoScreenState();
}

/*
Описание: State Экрана с информацией о доставке
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class _PackageInfoScreenState extends State<PackageInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text("Package Delivered", style: TextStyle(fontSize: 16, color: AppColors.grayColor)),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                Text(
                  "Package Information",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 8),
                Text(
                  "Origin details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.packageInfo.startAddress} ${widget.packageInfo.startCountry}",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.packageInfo.startPhone,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Destination details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  "${widget.packageInfo.endAddress} ${widget.packageInfo.endCountry}",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 4),
                Text(
                  widget.packageInfo.endPhone,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Other details",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).brightness == Brightness.dark ? AppColors.primaryVariantColor : AppColors.lightTextColor,
                  ),
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package Items",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.items,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Weight of items",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.weight,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tracking Number",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      widget.packageInfo.tracking_number,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 37),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 8),
                Text(
                  "Charges",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: AppColors.primaryColor),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Delivery Charges",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N2,500.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Instant delivery",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N300.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tax and Service Charges",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N200.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 9),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 1,
                      color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                    ))
                  ],
                ),
                SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Package total",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
                      ),
                    ),
                    Text(
                      'N3000.00',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 24),
                Text(
                  "Click on  delivered for successful delivery and rate rider or report missing item",
                  style: TextStyle(fontSize: 12, color: Color(0xFF2F80ED)),
                ),
                SizedBox(height: 41),
                Row(
                  children: [
                    Expanded(
                      child: AppButton(
                        onTap: () {},
                        text: "Report",
                        background: Theme.of(context).scaffoldBackgroundColor,
                        borderShape: AppShapes.outlinedButtonShape,
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: AppColors.primaryColor),
                      ),
                    ),
                    SizedBox(width: 24),
                    Expanded(
                      child: AppButton(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (_) => DeliverySuccessScreen(packageInfo: widget.packageInfo)));
                        },
                        text: "Successful",
                        background: AppColors.primaryColor,
                        borderShape: AppShapes.buttonShape,
                        textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w700, color: Colors.white),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
