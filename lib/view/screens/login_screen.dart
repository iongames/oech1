import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/screens/home_screen.dart';
import 'package:oech_1/view/screens/register_screen.dart';
import 'package:oech_1/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart';

import '../colors.dart';
import '../widgets/app_button.dart';
import '../widgets/app_text_field.dart';
import 'forgot_screen.dart';

/*
Дата создания: 27.06.2023
Описание: Класс экрана авторизации
Автор: Георгий Хасанов
*/
class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

/*
Дата создания: 27.06.2023
Описание: State Login Screen
Автор: Георгий Хасанов
*/
class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  bool _passwordVisible = false;
  bool _remember = false;

  late LoginViewModel viewModel;

  @override
  void initState() {
    super.initState();

    viewModel = Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 110),
                  Text(
                    "Welcome Back",
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Fill in your email and password to continue",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 48),
                  Text(
                    "Email Address",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: emailController,
                    placeholder: "***********@mail.com",
                    inputType: TextInputType.emailAddress,
                  ),
                  SizedBox(height: 24),
                  Text(
                    "Password",
                    style: TextStyle(fontSize: 14, fontFamily: "Roboto", fontWeight: FontWeight.w500, color: AppColors.grayColor),
                  ),
                  SizedBox(height: 8),
                  AppTextField(
                    controller: passwordController,
                    placeholder: "**********",
                    obscureText: _passwordVisible,
                    suffixIcon: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              _passwordVisible = !_passwordVisible;
                            });
                          },
                          child: SvgPicture.asset(
                            "assets/icons/ic_hide.svg",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Checkbox(
                        value: _remember,
                        onChanged: (value) {
                          setState(() {
                            _remember = !_remember;
                          });
                        },
                        activeColor: Theme.of(context).primaryColor,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
                      ),
                      Text("Remember password", style: Theme.of(context).textTheme.headlineSmall!.copyWith(fontSize: 12)),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => ForgotScreen()));
                    },
                    child: Text("Forgot Password?", style: Theme.of(context).textTheme.headlineSmall!.copyWith(fontSize: 12, color: AppColors.primaryColor)),
                  )
                ],
              ),
              SizedBox(height: 64),
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                            onTap: () async {
                              showDialog(
                                context: context,
                                builder: (builder) {
                                  return Center(child: CircularProgressIndicator());
                                },
                              );

                              bool result = await viewModel.logIn(emailController.text, passwordController.text);

                              if (result) {
                                Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                              }
                            },
                            text: "Log in"),
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  InkWell(
                    onTap: () async {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => const RegisterScreen()));
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(text: "Already have an account? ", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.grayColor)),
                        TextSpan(text: "Sign up", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.primaryColor)),
                      ]),
                    ),
                  )
                ],
              ),
              SizedBox(height: 30),
              Column(
                children: [
                  Text(
                    "or log in using",
                    style: Theme.of(context).textTheme.headlineMedium!.copyWith(fontWeight: FontWeight.w400),
                  ),
                  SizedBox(height: 8),
                  InkWell(
                    onTap: () async {
                      showDialog(
                        context: context,
                        builder: (builder) {
                          return Center(child: CircularProgressIndicator());
                        },
                      );

                      bool result = await viewModel.logInWithGoogle();

                      if (result) {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => HomeScreen()));
                      }
                    },
                    child: SvgPicture.asset("assets/icons/ic_google.svg"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    ));
  }
}
