import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/rider.dart';
import 'package:oech_1/view/screens/call_rider_screen.dart';
import 'package:oech_1/view/screens/chat_screen.dart';
import 'package:oech_1/view/shapes.dart';
import 'package:oech_1/view/widgets/app_button.dart';
import 'package:oech_1/view/widgets/app_text_field.dart';
import 'package:oech_1/view/widgets/review_item.dart';
import 'package:oech_1/view/widgets/rider_item.dart';

import '../colors.dart';

/*
Описание: Экран профиля доставщика
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class RiderScreen extends StatefulWidget {
  final Rider rider;

  const RiderScreen({super.key, required this.rider});

  @override
  State<RiderScreen> createState() => _RiderScreenState();
}

/*
Описание: State Экрана профиля доставщика
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class _RiderScreenState extends State<RiderScreen> {
  List<Rider> reviewList = [
    Rider("firstName", "lastName", "", "COMMENT", "", 0, "", "Online"),
    Rider("FIRST", "LAST", "", "COMMENT 2", "", 0, "", "Online"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: SvgPicture.asset(
              "assets/icons/ic_back.svg",
              width: 24,
              height: 24,
            ),
          ),
          automaticallyImplyLeading: false,
          title: Text(
            "Rider profile",
            style: TextStyle(
              fontSize: 16,
              color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor,
            ),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          elevation: 5,
          shadowColor: Colors.black.withOpacity(0.5),
        ),
        body: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Image.asset(
                "assets/images/image_map.png",
                height: 212,
                fit: BoxFit.cover,
                alignment: Alignment.bottomCenter,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 156),
                      Card(
                        elevation: 0,
                        margin: EdgeInsets.zero,
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                        child: Image.asset(
                          "assets/images/profile_default.png",
                          fit: BoxFit.cover,
                          width: 84,
                          height: 84,
                        ),
                      ),
                      SizedBox(height: 9),
                      Text(
                        "${widget.rider.lastName} ${widget.rider.firstName}",
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.w700,
                          color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.primaryColor,
                        ),
                      ),
                      SizedBox(height: 9),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(5, (index) {
                          return Padding(
                            padding: const EdgeInsets.all(4),
                            child: SvgPicture.asset(
                              "assets/icons/ic_star.svg",
                              color: index + 1 <= widget.rider.rating
                                  ? AppColors.primaryColor
                                  : Theme.of(context).brightness == Brightness.dark
                                      ? Colors.white
                                      : AppColors.grayColor,
                              width: 8,
                              height: 8,
                            ),
                          );
                        }),
                      ),
                      SizedBox(height: 16),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Text(
                                  "Car Model",
                                  style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  widget.rider.carModel,
                                  style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  "Registration Number",
                                  style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  widget.rider.regNumber,
                                  style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor),
                                )
                              ],
                            ),
                            Column(
                              children: [
                                Text(
                                  "Gender",
                                  style: TextStyle(fontSize: 12, color: AppColors.secondaryColor),
                                ),
                                SizedBox(height: 8),
                                Text(
                                  widget.rider.gender,
                                  style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.grayColor),
                                )
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 27),
                  Text(
                    "Customer Reviews",
                    style: TextStyle(fontSize: 12, color: Theme.of(context).hintColor),
                  ),
                  SizedBox(height: 8),
                  Container(
                    height: 180,
                    child: ListView.builder(
                      itemCount: reviewList.length,
                      shrinkWrap: true,
                      itemBuilder: (itemBuilder, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.4),
                          child: ReviewItem(rider: reviewList[index]),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 9),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            reviewList.add(Rider("firstName", "lastName", "", "СССС", "", 5, "", "Online"));
                          });
                        },
                        child: Text(
                          "View More",
                          style: TextStyle(fontSize: 12, color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.primaryColor),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  Row(
                    children: [
                      Expanded(
                        child: AppButton(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (_) => ChatScreen(rider: widget.rider)));
                          },
                          text: "Send Message",
                          padding: EdgeInsets.symmetric(vertical: 16),
                          textStyle: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(width: 24),
                      Expanded(
                        child: AppButton(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (_) => CallRiderScreen(rider: widget.rider)));
                          },
                          text: "Call Rider",
                          padding: EdgeInsets.symmetric(vertical: 16),
                          borderShape: AppShapes.outlinedButtonShape
                              .copyWith(side: BorderSide(color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.primaryColor)),
                          background: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
                          textStyle: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.primaryColor,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
