import 'package:flutter/cupertino.dart';

import 'colors.dart';

class AppShapes {
  static const outlinedButtonShape = RoundedRectangleBorder(
    side: BorderSide(width: 1, color: AppColors.primaryColor),
    borderRadius: BorderRadius.all(Radius.circular(4)),
  );

  static const buttonShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(4)),
  );
}
