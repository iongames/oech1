import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class UserCard extends StatelessWidget {
  final String name;

  const UserCard({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 50,
      child: Card(
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        clipBehavior: Clip.antiAlias,
        elevation: 0,
        child: Stack(
          children: [
            Image.asset(
              MediaQuery.of(context).platformBrightness == Brightness.light ? "assets/images/card_frame.png" : "assets/images/card_frame_dark.png",
              fit: BoxFit.cover,
            ),
            Positioned(
              top: 25,
              left: 12,
              right: 17,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Hello $name",
                        style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Colors.white),
                      ),
                      Text(
                        "We trust you are having a great time",
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white),
                      )
                    ],
                  ),
                  SvgPicture.asset(
                    "assets/icons/ic_notification.svg",
                    width: 24,
                    height: 24,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
