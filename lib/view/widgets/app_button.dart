import 'package:flutter/material.dart';
import 'package:oech_1/view/colors.dart';
import 'package:oech_1/view/shapes.dart';

class AppButton extends StatelessWidget {
  final String text;
  final Color? background;
  final TextStyle? textStyle;
  final ShapeBorder? borderShape;
  final Function() onTap;
  final EdgeInsets padding;

  const AppButton({
    super.key,
    required this.onTap,
    required this.text,
    this.textStyle,
    this.borderShape = AppShapes.buttonShape,
    this.background = AppColors.primaryColor,
    this.padding = const EdgeInsets.all(15),
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: background,
      shape: borderShape,
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: padding,
              child: Text(
                text,
                style: textStyle ?? Theme.of(context).textTheme.labelMedium,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
