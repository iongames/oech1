import 'package:flutter/material.dart';

import '../colors.dart';

/*
Описание: Элемент отслеживания заказа
Дата создания: 29.07.2023
Автор: Хасанов Георгий
*/
class StatusItem extends StatelessWidget {
  final bool value;
  final String title;
  final String date;
  final bool hasLine;

  const StatusItem({super.key, required this.value, required this.title, required this.date, required this.hasLine});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Stack(
        children: [
          hasLine
              ? Positioned(
                  left: 9,
                  top: 0,
                  bottom: 0,
                  child: Container(width: 1, color: AppColors.grayColor),
                )
              : SizedBox(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 20,
                height: 20,
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Checkbox(
                  value: value,
                  onChanged: (v) {},
                  activeColor: AppColors.primaryColor,
                  side: BorderSide(width: 2, color: AppColors.primaryColor),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2.33)),
                ),
              ),
              SizedBox(width: 7),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(fontSize: 14, color: value ? AppColors.primaryColor : AppColors.grayColor),
                  ),
                  SizedBox(height: 4),
                  Text(
                    date,
                    style: TextStyle(fontSize: 12, color: value ? AppColors.secondaryColor : AppColors.grayColor),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
