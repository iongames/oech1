import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../colors.dart';

class PaymentCard extends StatelessWidget {
  final String title;
  final String desc;

  const PaymentCard({super.key, required this.title, required this.desc});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.zero,
      shadowColor: Colors.black.withOpacity(0.5),
      color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
      shape: RoundedRectangleBorder(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 24),
        child: Row(
          children: [
            SvgPicture.asset("assets/icons/ic_radio.svg"),
            SizedBox(width: 8),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 16, color: Theme.of(context).hintColor),
                ),
                Text(
                  desc,
                  style: TextStyle(fontSize: 12, color: AppColors.grayColor),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
