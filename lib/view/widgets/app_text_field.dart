import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/view/colors.dart';

/*
Дата создания: 27.06.2023
Описание: Виджет текстового поля
Автор: Георгий Хасанов
*/
class AppTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final TextInputType? inputType;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;
  final bool obscureText;

  const AppTextField({
    super.key,
    required this.controller,
    this.onChanged,
    required this.placeholder,
    this.inputType,
    this.textInputAction,
    this.suffixIcon,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      obscureText: obscureText,
      controller: controller,
      keyboardType: inputType,
      textInputAction: textInputAction,
      style: Theme.of(context).textTheme.headlineMedium,
      decoration: InputDecoration(
        filled: true,
        suffixIcon: suffixIcon,
        fillColor: Theme.of(context).scaffoldBackgroundColor,
        focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: AppColors.grayColor)),
        enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: AppColors.grayColor)),
        hintText: placeholder,
        hintStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: AppColors.lightPlaceholderColor),
        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
      ),
    );
  }
}

/*
Дата создания: 27.06.2023
Описание: Виджет текстового поля
Автор: Георгий Хасанов
*/
class CodeTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;

  const CodeTextField({
    super.key,
    required this.controller,
    required this.placeholder,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: SizedBox(
        width: 32,
        height: 32,
        child: TextFormField(
          controller: controller,
          keyboardType: TextInputType.number,
          maxLength: 1,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
          decoration: InputDecoration(
            counterText: "",
            filled: true,
            fillColor: Theme.of(context).scaffoldBackgroundColor,
            focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: AppColors.secondaryColor), borderRadius: BorderRadius.zero),
            enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: AppColors.secondaryColor), borderRadius: BorderRadius.zero),
            hintText: placeholder,
            hintStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: AppColors.lightPlaceholderColor),
            contentPadding: EdgeInsets.zero,
          ),
        ),
      ),
    );
  }
}

/*
Дата создания: 27.06.2023
Описание: Виджет текстового поля
Автор: Георгий Хасанов
*/
class SearchTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String? icon;

  const SearchTextField({
    super.key,
    required this.controller,
    required this.placeholder,
    this.onChanged,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      controller: controller,
      keyboardType: TextInputType.number,
      maxLines: 1,
      style: Theme.of(context).textTheme.headlineMedium,
      decoration: InputDecoration(
          counterText: "",
          filled: true,
          fillColor: Theme.of(context).cardColor,
          focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
          enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
          hintText: placeholder,
          hintStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: AppColors.grayColor),
          contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 9),
          suffixIcon: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon ?? "assets/icons/ic_search.svg",
                color: AppColors.grayColor,
              )
            ],
          )),
    );
  }
}

/*
Дата создания: 27.06.2023
Описание: Виджет текстового поля отправки
Автор: Георгий Хасанов
*/
class PackageTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;
  final Function(String)? onChanged;

  const PackageTextField({
    super.key,
    required this.controller,
    required this.placeholder,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: RoundedRectangleBorder(),
      margin: EdgeInsets.zero,
      child: TextFormField(
        onChanged: onChanged,
        controller: controller,
        keyboardType: TextInputType.number,
        maxLines: 1,
        style: Theme.of(context).textTheme.headlineMedium,
        decoration: InputDecoration(
          counterText: "",
          filled: true,
          fillColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.zero),
          enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.zero),
          hintText: placeholder,
          hintStyle: TextStyle(color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightPlaceholderColor, fontSize: 12),
          contentPadding: EdgeInsets.symmetric(horizontal: 8),
        ),
      ),
    );
  }
}

/*
Дата создания: 29.06.2023
Описание: Виджет текстового поля отзыва
Автор: Георгий Хасанов
*/
class FeedbackTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;
  final Function(String)? onChanged;

  const FeedbackTextField({
    super.key,
    required this.controller,
    required this.placeholder,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: RoundedRectangleBorder(),
      margin: EdgeInsets.zero,
      child: TextFormField(
        onChanged: onChanged,
        controller: controller,
        keyboardType: TextInputType.number,
        maxLines: 1,
        maxLength: 10,
        style: Theme.of(context).textTheme.headlineMedium,
        decoration: InputDecoration(
          prefixIcon: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12),
                child: SvgPicture.asset("assets/icons/ic_feedback.svg"),
              ),
            ],
          ),
          counterText: "",
          filled: true,
          fillColor: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
          focusedBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.zero),
          enabledBorder: const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.zero),
          hintText: placeholder,
          hintStyle: TextStyle(color: Theme.of(context).brightness == Brightness.dark ? Colors.white : AppColors.lightPlaceholderColor, fontSize: 12),
          contentPadding: EdgeInsets.symmetric(vertical: 17, horizontal: 12),
        ),
      ),
    );
  }
}

/*
Дата создания: 30.06.2023
Описание: Виджет текстового поля
Автор: Георгий Хасанов
*/
class S5TextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController? controller;
  final Function(String)? onChanged;
  final String? icon;

  const S5TextField({
    super.key,
    required this.controller,
    required this.placeholder,
    this.onChanged,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      controller: controller,
      keyboardType: TextInputType.number,
      maxLines: 1,
      style: Theme.of(context).textTheme.headlineMedium,
      decoration: InputDecoration(
          counterText: "",
          filled: true,
          fillColor: Theme.of(context).brightness == Brightness.dark
              ? Theme.of(context).cardColor
              : controller!.text.isNotEmpty
                  ? Colors.white
                  : Theme.of(context).cardColor,
          focusedBorder: Theme.of(context).brightness == Brightness.dark
              ? const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.all(Radius.circular(8)))
              : controller!.text.isNotEmpty
                  ? const OutlineInputBorder(borderSide: BorderSide(color: AppColors.lightPlaceholderColor), borderRadius: BorderRadius.all(Radius.circular(8)))
                  : const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.all(Radius.circular(8))),
          enabledBorder: Theme.of(context).brightness == Brightness.dark
              ? const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.all(Radius.circular(8)))
              : controller!.text.isNotEmpty
                  ? const OutlineInputBorder(borderSide: BorderSide(color: AppColors.lightPlaceholderColor), borderRadius: BorderRadius.all(Radius.circular(8)))
                  : const OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent), borderRadius: BorderRadius.all(Radius.circular(8))),
          hintText: placeholder,
          hintStyle: Theme.of(context).textTheme.headlineMedium!.copyWith(color: AppColors.grayColor),
          contentPadding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
          suffixIcon: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon ?? "assets/icons/ic_search.svg",
                color: AppColors.grayColor,
              )
            ],
          )),
    );
  }
}
