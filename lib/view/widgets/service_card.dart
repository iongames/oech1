import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/service_item.dart';

import '../colors.dart';

class ServiceCard extends StatefulWidget {
  final ServiceItem serviceItem;
  final Function() onTap;

  const ServiceCard({super.key, required this.serviceItem, required this.onTap});

  @override
  State<StatefulWidget> createState() => ServiceCardState();
}

class ServiceCardState extends State<ServiceCard> {
  late Color cardColor;
  late Color titleColor;
  late Color textColor;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    cardColor = Theme.of(context).brightness == Brightness.light ? Color(0xFFF2F2F2) : AppColors.darkPlaceholderColor;
    titleColor = Theme.of(context).primaryColor;
    textColor = Theme.of(context).hintColor;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      margin: EdgeInsets.all(12),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: cardColor,
      child: InkWell(
        splashFactory: NoSplash.splashFactory,
        onTap: widget.onTap,
        onTapDown: (value) {
          setState(() {
            cardColor = Theme.of(context).primaryColor;
            titleColor = Colors.white;
            textColor = Colors.white;
          });
        },
        onTapUp: (value) {
          setState(() {
            cardColor = Theme.of(context).brightness == Brightness.light ? Color(0xFFF2F2F2) : AppColors.darkPlaceholderColor;
            titleColor = Theme.of(context).primaryColor;
            textColor = Theme.of(context).hintColor;
          });
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 22),
              SvgPicture.asset(
                widget.serviceItem.asset,
                color: titleColor,
              ),
              SizedBox(height: 12),
              Text(
                widget.serviceItem.title,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: titleColor),
              ),
              SizedBox(height: 5),
              Text(
                widget.serviceItem.content,
                style: TextStyle(fontSize: 8, fontWeight: FontWeight.w400, color: textColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
