import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../colors.dart';

/*
Описание: Инфоврмация о пользвоае ьлц
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class ProfileCard extends StatelessWidget {
  final String balance;
  final bool hide;
  final Function() onIconTap;

  const ProfileCard({super.key, required this.balance, required this.hide, required this.onIconTap});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Card(
              elevation: 0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000), side: BorderSide(width: 1, color: AppColors.lightPlaceholderColor)),
              color: Colors.transparent,
              clipBehavior: Clip.antiAlias,
              child: Image.asset(
                "assets/images/profile_default.png",
              ),
            ),
            SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Ken Nwaeze",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                ),
                RichText(
                    text: TextSpan(children: [
                  TextSpan(text: "Current balance: ", style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor)),
                  TextSpan(text: !hide ? balance : "*******", style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: AppColors.primaryColor)),
                ]))
              ],
            ),
          ],
        ),
        InkWell(
          onTap: onIconTap,
          child: SvgPicture.asset("assets/icons/ic_hide.svg"),
        )
      ],
    );
  }
}

/*
Описание: Карточка профиля пользователя
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class ItemCard extends StatelessWidget {
  final String asset;
  final String title;
  final String desc;
  final Function() onTap;

  const ItemCard({super.key, required this.asset, required this.title, required this.desc, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
      shape: const RoundedRectangleBorder(),
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(13),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    asset,
                    color: Theme.of(context).hintColor,
                  ),
                  SizedBox(width: 9),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                      ),
                      SizedBox(height: 4),
                      Text(
                        desc,
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                      ),
                    ],
                  ),
                ],
              ),
              SvgPicture.asset("assets/icons/ic_next_ios.svg")
            ],
          ),
        ),
      ),
    );
  }
}

/*
Описание: Карточка выхода из аккаунта
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class LogOutCard extends StatelessWidget {
  final Function() onTap;

  const LogOutCard({super.key, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: const RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(13),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/icons/ic_logout.svg",
                    color: Color(0xFFED3A3A),
                  ),
                  SizedBox(width: 9),
                  Text(
                    "Log Out",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                  ),
                ],
              ),
              SvgPicture.asset("assets/icons/ic_next_ios.svg")
            ],
          ),
        ),
      ),
    );
  }
}

/*
Описание: Карточка транзакции в кошельке
Дата создания: 28.07.2023
Автор: Хасанов Георгий
*/
class TransactionCard extends StatelessWidget {
  final String title;
  final String date;
  final String sum;
  final bool type;

  const TransactionCard({super.key, required this.title, required this.date, required this.sum, required this.type});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
      shape: const RoundedRectangleBorder(),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 9),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                    ),
                    SizedBox(height: 4),
                    Text(
                      date,
                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: AppColors.grayColor),
                    ),
                  ],
                ),
              ],
            ),
            Text(
              sum,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: type ? Color(0xFF35B369) : Color(0xFFED3A3A)),
            ),
          ],
        ),
      ),
    );
  }
}
