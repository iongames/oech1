import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:oech_1/model/rider.dart';

import '../colors.dart';

/*
Описание: Элемент с информацией о доставщике в списке
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class RiderItem extends StatelessWidget {
  final Rider rider;
  final Function() onTap;

  const RiderItem({super.key, required this.rider, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 5,
      shadowColor: Colors.black.withOpacity(0.5),
      shape: RoundedRectangleBorder(),
      color: Theme.of(context).brightness == Brightness.dark ? AppColors.darkPlaceholderColor : Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    child: Card(
                      elevation: 0,
                      margin: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset("assets/images/profile_default.png"),
                    ),
                  ),
                  SizedBox(width: 12),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${rider.lastName} ${rider.firstName}",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Theme.of(context).hintColor),
                      ),
                      SizedBox(height: 4),
                      Text(
                        rider.regNumber,
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Theme.of(context).hintColor),
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: List.generate(5, (index) {
                      return Padding(
                        padding: const EdgeInsets.all(4),
                        child: SvgPicture.asset(
                          "assets/icons/ic_star.svg",
                          color: index + 1 <= rider.rating
                              ? AppColors.primaryColor
                              : Theme.of(context).brightness == Brightness.dark
                                  ? Colors.white
                                  : AppColors.grayColor,
                          width: 8,
                          height: 8,
                        ),
                      );
                    }),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
