import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:oech_1/view/colors.dart';

/*
Описание: Элемент экрана чата 
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class ChatItem extends StatelessWidget {
  final String text;
  final bool type;

  const ChatItem({super.key, required this.type, required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: type
          ? BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6)
          : BoxConstraints(minWidth: MediaQuery.of(context).size.width * 0.6, maxWidth: MediaQuery.of(context).size.width * 0.6),
      child: Card(
        margin: EdgeInsets.zero,
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        color: type
            ? AppColors.primaryColor
            : Theme.of(context).brightness == Brightness.dark
                ? AppColors.darkPlaceholderColor
                : AppColors.lightPlaceholderColor,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            text,
            style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
                color: type
                    ? Colors.white
                    : Theme.of(context).brightness == Brightness.dark
                        ? Colors.white
                        : AppColors.lightTextColor),
          ),
        ),
      ),
    );
  }
}
