import 'package:flutter/material.dart';
import 'package:oech_1/model/onboard_item.dart';
import 'package:oech_1/view/colors.dart';
import 'package:oech_1/view/screens/login_screen.dart';
import 'package:oech_1/view/screens/register_screen.dart';
import 'package:oech_1/view/shapes.dart';
import 'package:oech_1/view/widgets/app_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardComponent extends StatelessWidget {
  final OnboardItem onboardItem;
  final int itemIndex;

  const OnboardComponent({super.key, required this.onboardItem, required this.itemIndex});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            itemIndex == 0
                ? const SizedBox(height: 66)
                : itemIndex == 1
                    ? const SizedBox(height: 88)
                    : const SizedBox(height: 105),
            Image.asset(onboardItem.asset),
            itemIndex == 0
                ? const SizedBox(height: 48)
                : itemIndex == 1
                    ? const SizedBox(height: 62)
                    : const SizedBox(height: 63),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22),
              child: Column(
                children: [
                  Text(
                    onboardItem.title,
                    style: Theme.of(context).textTheme.displayMedium,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    onboardItem.content,
                    style: Theme.of(context).textTheme.displaySmall,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 43),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                3,
                (index) {
                  return Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1000)),
                    color: itemIndex == index ? Theme.of(context).primaryColor : AppColors.grayColor,
                    clipBehavior: Clip.antiAlias,
                    child: const SizedBox(height: 9, width: 9),
                  );
                },
              ),
            ),
            const SizedBox(height: 82),
            itemIndex != 2
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AppButton(
                        key: itemIndex == 0 ? const Key("skip1") : const Key("skip2"),
                        onTap: () async {
                          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

                          await sharedPreferences.setBool("isFirstEnter", false);

                          Navigator.push(context, MaterialPageRoute(builder: (_) => const RegisterScreen()));
                        },
                        text: "Skip",
                        background: Theme.of(context).scaffoldBackgroundColor,
                        borderShape: AppShapes.outlinedButtonShape,
                        textStyle: Theme.of(context).textTheme.labelSmall!.copyWith(color: AppColors.primaryColor),
                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                      ),
                      AppButton(
                        onTap: () {},
                        text: "Next",
                        borderShape: AppShapes.buttonShape,
                        textStyle: Theme.of(context).textTheme.labelSmall!.copyWith(color: Colors.white),
                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                      ),
                    ],
                  )
                : Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: AppButton(
                                onTap: () async {
                                  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

                                  await sharedPreferences.setBool("isFirstEnter", false);

                                  Navigator.push(context, MaterialPageRoute(builder: (_) => const RegisterScreen()));
                                },
                                text: "Sign Up"),
                          )
                        ],
                      ),
                      const SizedBox(height: 8),
                      InkWell(
                        onTap: () async {
                          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

                          await sharedPreferences.setBool("isFirstEnter", false);

                          Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                        },
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(text: "Already have an account? ", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.grayColor)),
                            TextSpan(text: "Sign in", style: Theme.of(context).textTheme.displaySmall!.copyWith(color: AppColors.primaryColor)),
                          ]),
                        ),
                      )
                    ],
                  )
          ],
        ),
      ),
    );
  }
}
