import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xFF0560FA);
  static const primaryVariantColor = Color(0xFFBADAFF);
  static const secondaryColor = Color(0xFFEC8000);
  static const grayColor = Color(0xFFA7A7A7);
  static const warningColor = Color(0xFFEBBC2E);

  static const lightTextColor = Color(0xFF3A3A3A);
  static const lightPlaceholderColor = Color(0xFFCFCFCF);
  static const darkTextColor = Color(0xFFFFFFFF);

  static const darkBackgroundColor = Color(0xFF000D1D);
  static const darkPlaceholderColor = Color(0xFF001B3B);
}
