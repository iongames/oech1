class PackageInfo {
  final String startAddress;
  final String startCountry;
  final String startPhone;
  final String startOthers;

  final String endAddress;
  final String endCountry;
  final String endPhone;
  final String endOthers;

  final String items;
  final String weight;
  final String tracking_number;

  PackageInfo(this.startAddress, this.startCountry, this.startPhone, this.startOthers, this.endAddress, this.endCountry, this.endPhone, this.endOthers,
      this.items, this.weight, this.tracking_number);
}
