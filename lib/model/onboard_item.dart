class OnboardItem {
  final String title;
  final String content;
  final String asset;

  OnboardItem(this.title, this.content, this.asset);
}
