/*
Описание: Модель сообщения
Дата создания: 30.07.2023
Автор: Хасанов Георгий
*/
class Message {
  final String message;
  final bool type;

  Message(this.message, this.type);
}
