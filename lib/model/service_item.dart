class ServiceItem {
  final String title;
  final String content;
  final String asset;

  ServiceItem(this.title, this.content, this.asset);
}
