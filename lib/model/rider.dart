/*
Описание: Модель ифнормации о доставщике
Дата создания: 20.07.2023
Автор: Хасанов Георгий
*/
class Rider {
  final String firstName;
  final String lastName;
  final String carModel;
  final String regNumber;
  final String gender;
  final double rating;
  final String riderNumber;
  final String online;

  Rider(this.firstName, this.lastName, this.carModel, this.regNumber, this.gender, this.rating, this.riderNumber, this.online);
}
