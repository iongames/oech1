import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:oech_1/view/colors.dart';
import 'package:oech_1/view/screens/home_screen.dart';
import 'package:oech_1/view/screens/login_screen.dart';
import 'package:oech_1/view/screens/onboard_screen.dart';
import 'package:oech_1/view/screens/register_screen.dart';
import 'package:oech_1/viewmodel/home_viewmodel.dart';
import 'package:oech_1/viewmodel/login_viewmodel.dart';
import 'package:provider/provider.dart' as provider;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: "https://qqdruecqbscnicnedwnv.supabase.co",
    anonKey:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFxZHJ1ZWNxYnNjbmljbmVkd252Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODc5NDExNzAsImV4cCI6MjAwMzUxNzE3MH0.Q-dQdV0c20KlRh5waol13mSfklW0DKnfqU37YLTDMX8",
    authFlowType: AuthFlowType.pkce,
  );

  runApp(
    provider.MultiProvider(
      providers: [
        provider.ChangeNotifierProvider(
          create: (c) => LoginViewModel(),
        ),
        provider.ChangeNotifierProvider(
          create: (c) => HomeViewModel(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  late LoginViewModel viewModel;

  Future<Widget> getSharedPrefs() async {
    SharedPreferences shared = await SharedPreferences.getInstance();

    if (shared.getBool("isFirstEnter") != false) {
      return OnboardScreen();
    } else {
      if (await viewModel.checkUser()) {
        return HomeScreen();
      } else {
        return LoginScreen();
      }
    }
  }

  @override
  void initState() {
    super.initState();

    viewModel = provider.Provider.of(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'OECH',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(
        useMaterial3: false,
        primaryColor: AppColors.primaryColor,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        cardColor: AppColors.lightPlaceholderColor,
        hintColor: AppColors.lightTextColor,
        textTheme: const TextTheme().copyWith(
          displayMedium: const TextStyle(fontSize: 24, fontFamily: "Roboto", color: AppColors.primaryColor, fontWeight: FontWeight.w700),
          displaySmall: const TextStyle(fontSize: 16, fontFamily: "Roboto", color: AppColors.lightTextColor, fontWeight: FontWeight.w400),
          labelMedium: const TextStyle(fontSize: 16, fontFamily: "Roboto", color: Colors.white, fontWeight: FontWeight.w700),
          labelSmall: const TextStyle(fontSize: 9, fontFamily: "Roboto", color: AppColors.primaryColor, fontWeight: FontWeight.w700),
          headlineLarge: const TextStyle(fontSize: 24, fontFamily: "Roboto", color: AppColors.lightTextColor, fontWeight: FontWeight.w500),
          headlineMedium: const TextStyle(fontSize: 14, fontFamily: "Roboto", color: AppColors.lightTextColor, fontWeight: FontWeight.w500),
          headlineSmall: const TextStyle(fontSize: 14, fontFamily: "Roboto", color: AppColors.grayColor, fontWeight: FontWeight.w500),
        ),
        fontFamily: "Roboto",
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: AppColors.primaryColor,
        backgroundColor: AppColors.darkBackgroundColor,
        cardColor: AppColors.darkPlaceholderColor,
        hintColor: AppColors.darkTextColor,
        scaffoldBackgroundColor: AppColors.darkBackgroundColor,
        textTheme: const TextTheme().copyWith(
          displayMedium: const TextStyle(fontSize: 24, fontFamily: "Roboto", color: AppColors.primaryColor, fontWeight: FontWeight.w700),
          displaySmall: const TextStyle(fontSize: 16, fontFamily: "Roboto", color: AppColors.darkTextColor, fontWeight: FontWeight.w400),
          labelMedium: const TextStyle(fontSize: 16, fontFamily: "Roboto", color: Colors.white, fontWeight: FontWeight.w700),
          labelSmall: const TextStyle(fontSize: 9, fontFamily: "Roboto", color: AppColors.primaryColor, fontWeight: FontWeight.w700),
          headlineLarge: const TextStyle(fontSize: 24, fontFamily: "Roboto", color: Colors.white, fontWeight: FontWeight.w500),
          headlineMedium: const TextStyle(fontSize: 14, fontFamily: "Roboto", color: Colors.white, fontWeight: FontWeight.w500),
          headlineSmall: const TextStyle(fontSize: 14, fontFamily: "Roboto", color: Colors.white, fontWeight: FontWeight.w500),
        ),
        fontFamily: "Roboto",
      ),
      home: FutureBuilder(
        future: getSharedPrefs(),
        builder: (builder, snapshot) {
          if (snapshot.hasData) {
            return snapshot.data!;
          } else {
            return SizedBox();
          }
        },
      ),
    );
  }
}
